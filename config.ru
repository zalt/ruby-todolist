require 'sinatra'
require 'opal'
require 'opal-jquery'
require_relative 'app'

map '/assets' do
  env = Opal::Environment.new
  env.append_path 'opal'
  run env
end

run Sinatra::Application

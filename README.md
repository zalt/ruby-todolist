# Todo list app using [Opal](http://opalrb.org/)

This is an imitation of [that](http://izm-11.hatenablog.com/entry/2014/02/15/012437).

## Installation

    $ git clone http://bitbucket.org/zalt/ruby-todolist.git
    $ cd ruby-todolist
    $ bundle install --path vendor/bundle

## Usage

    $ bundle exec rackup

and open [http://localhost:9292](http://localhost:9292).

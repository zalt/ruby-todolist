require 'opal'
require 'opal-jquery'
require 'erb'

Document.ready? do
  Element['.done'].each do |done|
    done.on(:click) do |e|
      HTTP.post('/done', data: { id: done.parents('li').id }) do |r|
        if r.ok?
          data = r.json
          if data[:status] == 'done'
            Element["##{data[:id]} a.done"].text = 'Not done'
            Element["##{data[:id]} .item"].html =
              '<del>' + ERB::Util.html_escape(Element["##{data[:id]} .item"].text) + '</del>'
          else
            Element["##{data[:id]} a.done"].text = 'Done'
            Element["##{data[:id]} .item"].html =
              ERB::Util.html_escape(Element["##{data[:id]} .item"].text)
          end
        end
      end
      e.prevent_default
    end
  end
end
